﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookForPlayer : MonoBehaviour {

    private AudioSource audioSource;
    private bool hasPlayed;

	// Use this for initialization
	void Start ()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!hasPlayed)
        {
            Collider[] cols = Physics.OverlapSphere(transform.position, 20f);

            foreach (Collider col in cols)
            {
                if (col.gameObject.tag == "Player")
                {
                    audioSource.Play();
                    hasPlayed = true;
                }
            }
        }
	}
}
