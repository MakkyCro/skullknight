﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayOnBossDeath : MonoBehaviour {

    private AudioSource audioSource;
    private bool hasPlayed;
    public float delayBeforePlaying = 1.5f;
    public GameObject victoryCanvas;

    // Use this for initialization
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasPlayed)
        {
            GameObject boss = GameObject.FindGameObjectWithTag("Boss");

            if (boss == null)
            {

                if (delayBeforePlaying < 0f)
                {
                    audioSource.Play();
                    hasPlayed = true;
                }
                else
                    delayBeforePlaying -= Time.deltaTime;
            }
        }
        else if (!audioSource.isPlaying && hasPlayed && !victoryCanvas.activeSelf)
        {
            victoryCanvas.SetActive(true);
        }
        else if (!audioSource.isPlaying && hasPlayed && victoryCanvas.activeSelf)
        {
            if (Input.GetKeyDown("space"))
            {
                victoryCanvas.SetActive(false);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                SceneManager.LoadScene(0);
            }
        }
    }
}
