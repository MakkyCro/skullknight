using UnityEngine;

public class ItemPickup : Interactable {

	public Item item;	// Item to put in the inventory if picked up

	// Player interacts with item
	public override void Interact()
	{
		base.Interact();

		PickUp();
	}

	// Picking up the item
	void PickUp ()
	{
		Debug.Log("Picking up " + item.name);
		Inventory.instance.Add(item);	// Add to inventory

		Destroy(gameObject);	// Destroy item from scene
	}

}
