using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This makes our enemy interactable. */

[RequireComponent(typeof(CharacterStats))]
public class Enemy : Interactable {

	CharacterStats stats;
	public RagdollManager ragdoll;
    private bool TookDmg = false;

	void Start ()
	{
		stats = GetComponent<CharacterStats>();
		stats.OnHealthReachedZero += Die;
	}

	// When we interact with the enemy: We attack it decreasing health stat
	public override void Interact()
	{
		CharacterCombat combatManager = Player.instance.playerCombatManager;
		combatManager.Attack(stats);
	}

    // If health reaches zero
	void Die()
    {
		if(ragdoll != null)
        {
            ragdoll.transform.parent = null;
            ragdoll.Setup();
        }
		Destroy(gameObject);
	}

    private void OnTriggerEnter(Collider other)
    {
        if(!TookDmg && PlayerController.Attacked)
        {
            if (other.gameObject.tag == "RHand" || other.gameObject.tag == "PlayerSword")
            {
                Player player = other.GetComponentInParent<Player>();
                stats.TakeDamage(player.playerStats.damage.GetValue());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.tag == "RHand" || other.gameObject.tag == "PlayerSword")
        {
            TookDmg = false;
        }
    }

}
