using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Consumable")]
public class Consumable : Item {

	public int healthGain;		// Number of health points

	// After pressing in inventory
	public override void Use()
	{
		// Heal the player
		PlayerStats playerStats = Player.instance.playerStats;
		playerStats.Heal(healthGain);

		Debug.Log(name + " consumed!");

		RemoveFromInventory();	// Remove the item after using
	}

}
