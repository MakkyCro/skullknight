using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EquipmentManager : MonoBehaviour
{

	#region Singleton


	public static EquipmentManager instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<EquipmentManager> ();
			}
			return _instance;
		}
	}
	static EquipmentManager _instance;

	void Awake ()
	{
		_instance = this;
	}

	#endregion

	public Equipment[] defaultWear;
    private Transform rHand;

	Equipment[] currentEquipment;
	SkinnedMeshRenderer[] currentMeshes;

	public SkinnedMeshRenderer targetMesh;

	// Callback for when an item is equipped
	public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
	public event OnEquipmentChanged onEquipmentChanged;

	Inventory inventory;

	void Start()
	{
		inventory = Inventory.instance;

		int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
		currentEquipment = new Equipment[numSlots];
		currentMeshes = new SkinnedMeshRenderer[numSlots];

        rHand = GameObject.FindGameObjectWithTag("RHand").transform;

		EquipAllDefault();
	}

    // Unequiping all equipment on U key
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.U))
			UnequipAll();
	}


	public Equipment GetEquipment(EquipmentSlot slot) {
		return currentEquipment[(int)slot];
	}

	// Equip a new item
	public void Equip(Equipment newItem)
	{
		Equipment oldItem = null;

        // Find out what slot the item fits in  and put it there.
        int slotIndex = (int)newItem.equipSlot;

        // If there was already an item in the slot put it back in the inventory
        if (currentEquipment[slotIndex] != null)
		{
			oldItem = currentEquipment[slotIndex];
			inventory.Add(oldItem);
		}

		// An item has been equipped so we trigger the callback
		if (onEquipmentChanged != null)
			onEquipmentChanged.Invoke(newItem, oldItem);

		currentEquipment[slotIndex] = newItem;
		Debug.Log(newItem.name + " equipped!");

		if (newItem.prefab)
			AttachToMesh(newItem.prefab, slotIndex);
	}

	void Unequip(int slotIndex)
    {
		if (currentEquipment[slotIndex] != null)
		{
			Equipment oldItem = currentEquipment[slotIndex];
			inventory.Add(oldItem);
				
			currentEquipment[slotIndex] = null;
			if (currentMeshes[slotIndex] != null)
				Destroy(currentMeshes[slotIndex].gameObject);

			// Equipment has been removed so we trigger the callback
			if (onEquipmentChanged != null)
				onEquipmentChanged.Invoke(null, oldItem);
		}
	}

	void UnequipAll()
    {
		for (int i = 0; i < currentEquipment.Length; i++)
			Unequip(i);
		EquipAllDefault();
	}

    // Equiping default items
	void EquipAllDefault()
    {
		foreach (Equipment e in defaultWear)
			Equip(e);
        BoxCollider col = rHand.GetComponent<BoxCollider>();
        if (col != null)
            col.enabled = true;
    }

	void AttachToMesh(SkinnedMeshRenderer mesh, int slotIndex) {

        SkinnedMeshRenderer newMesh;

		if (currentMeshes[slotIndex] != null)
			Destroy(currentMeshes[slotIndex].gameObject);

        if (slotIndex == 3)
        {
            newMesh = Instantiate(mesh, rHand) as SkinnedMeshRenderer;
            BoxCollider col = rHand.GetComponent<BoxCollider>();
            if (col != null)
                col.enabled = false;
        }
        else
		    newMesh = Instantiate(mesh) as SkinnedMeshRenderer;

        newMesh.bones = targetMesh.bones;
		newMesh.rootBone = targetMesh.rootBone;
		currentMeshes[slotIndex] = newMesh;
	}

}
