﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PortalAnimation : MonoBehaviour {

    public Transform portalStones;
    public Transform portalStones2;

    public float RotationSpeed = 4f;
    public int sceneToLoad;

    public float delay = 2f;
    public float timeLeft;

    private bool startedLoading = false;
    private ParticleSystem _particleSystem;
    private float rotationSpeed = 0f;
    private bool animated = false;

	void Start()
    {
        _particleSystem = GetComponentInChildren<ParticleSystem>();
    }
	
	void FixedUpdate()
    {
        if (animated)
        {
            _particleSystem.Play();
            timeLeft -= Time.fixedDeltaTime;
            if (portalStones.localPosition.y < 1)
            {
                portalStones.Translate(new Vector3(0, 1, 0) * Time.fixedDeltaTime, Space.World);
            }
            else
            {
                if (!startedLoading && timeLeft < 0f)
                {
                    startedLoading = true;
                    StartCoroutine(LoadYourAsyncScene());
                }
            }
            if(portalStones2.localPosition.y < 1.4)
            {
                portalStones2.Translate(new Vector3(0, 1.4f, 0) * Time.fixedDeltaTime, Space.World);
            }
        }
        else if(!animated)
        {
            if (portalStones.localPosition.y > 0)
            {
                portalStones.Translate(new Vector3(0, -1, 0) * Time.fixedDeltaTime, Space.World);
                _particleSystem.Stop();
            }
            else if (portalStones.localPosition.y < 0)
            {
                portalStones.localPosition = new Vector3(0, 0, 0);
            }
            if (portalStones2.localPosition.y > 0)
            {
                portalStones2.Translate(new Vector3(0, -1.4f, 0) * Time.fixedDeltaTime, Space.World);
            }
            else if (portalStones2.localPosition.y < 0)
            {
                portalStones2.localPosition = new Vector3(0, 0, 0);
            }
        }

        rotationSpeed = RotationSpeed * portalStones.localPosition.y;
        if(animated)
        {
            portalStones.Rotate(0f, rotationSpeed, 0f, Space.World);
            portalStones2.Rotate(0f, -rotationSpeed, 0f, Space.World);
        }
        else if (!animated && portalStones.localPosition.y > 0.00000001f)
        {
            portalStones.localEulerAngles = portalStones2.localEulerAngles - Quaternion.Euler(0, 0, 45 * (1f - portalStones.localPosition.y)).eulerAngles * 0.4f;
            portalStones2.localEulerAngles = portalStones.localEulerAngles - Quaternion.Euler(0, 0, 45 * (1f - portalStones.localPosition.y)).eulerAngles * 0.4f;
        }
        else
        {
            portalStones.localEulerAngles = portalStones2.localEulerAngles - Quaternion.Euler(0, 0, 45).eulerAngles;
        }
    }

    public void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            animated = true;
            timeLeft = delay;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            animated = false;
        }
    }

    IEnumerator LoadYourAsyncScene()
    {
        // The Application loads the Scene in the background at the same time as the current Scene.
        //This is particularly good for creating loading screens. You could also load the Scene by build //number.
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad);

        //Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        //SceneManager.SetActiveScene(sceneToLoad);
    }
}
