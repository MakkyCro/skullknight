using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/* This class just makes it faster to get certain components on the player. */


public class Player : MonoBehaviour {

	#region Singleton

	public static Player instance;

	void Awake ()
	{
		instance = this;
	}

    #endregion

    public CharacterCombat playerCombatManager;
    public PlayerStats playerStats;
    public int healAmountPerSecond = 1;
    private float secondDelay = 0;
    public float outOfCombat = 5f;

    [SerializeField]
    public GameObject gameOverObject;


    void Start()
    {
		playerStats.OnHealthReachedZero += Die;
    }
    private void Update()
    {
        secondDelay += Time.deltaTime;
        if (Input.GetKeyDown("space") && gameOverObject.activeSelf)
        {
            gameOverObject.SetActive(false);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1;
        }
        if(secondDelay > 1f)
        {
            playerStats.Heal(healAmountPerSecond);
            secondDelay = 0f;
        }

        if (outOfCombat < 0f)
        {
            playerStats.Heal(playerStats.maxHealth.GetValue());
        }
        else
            outOfCombat -= Time.deltaTime;

    }

	void Die()
    {
        Time.timeScale = 0f;
        gameOverObject.SetActive(true);      
    
	}
}
