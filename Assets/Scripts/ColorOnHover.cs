using UnityEngine;
using System.Linq;


public class ColorOnHover : MonoBehaviour
{

	public Color color;
	public Renderer meshRenderer;
    public bool Highlighted = false;

	Color[] originalColours;

	void Start()
    {
		if (meshRenderer == null)
        {
			meshRenderer = GetComponent<MeshRenderer> ();
		}
		originalColours = meshRenderer.materials.Select(x => x.color).ToArray();
	}

	public void Highlight()
	{
        if(!Highlighted)
        {
		    foreach (Material mat in meshRenderer.materials)
			    mat.color *= color;
            Highlighted = true;
        }
	}

	public void DeHighlight()
	{
        if(Highlighted)
        {
            for (int i = 0; i < originalColours.Length; i++)
                meshRenderer.materials[i].color = originalColours[i];
            Highlighted = false;
        }
	}
}
