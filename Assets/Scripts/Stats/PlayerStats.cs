using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : CharacterStats {

    private Player player;

	// Use this for initialization
	public override void Start()
    {
		base.Start();
		EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
        player = gameObject.GetComponent<Player>();
	}

	void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
	{
		if (newItem != null)
        {
			armor.AddModifier(newItem.armorModifier);
			damage.AddModifier(newItem.damageModifier);
		}

		if (oldItem != null)
		{
			armor.RemoveModifier(oldItem.armorModifier);
			damage.RemoveModifier(oldItem.armorModifier);
		}
	}

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
        player.outOfCombat = 5f;
    }
}
