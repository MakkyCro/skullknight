﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalAnimationInsideMap : MonoBehaviour
{

    public Transform portalStones;
    public Transform portalStones2;
    public float RotationSpeed = 2f;
    public Transform teleportPoint;
    public float delay = 2f;

    private float timeLeft;

    private ParticleSystem _particleSystem;
    private float rotationSpeed = 0f;
    private bool animated = false;
    private GameObject player;

    void Start()
    {
        _particleSystem = GetComponentInChildren<ParticleSystem>();
    }

    void FixedUpdate()
    {
        if (animated)
        {
            _particleSystem.Play();
            timeLeft -= Time.fixedDeltaTime;
            if (portalStones.localPosition.y < 1)
            {
                portalStones.Translate(new Vector3(0, 1, 0) * Time.fixedDeltaTime, Space.World);
            }
            else
            {
                if(timeLeft < 0)
                {
                    UnityEngine.AI.NavMeshAgent meshAgent = player.GetComponent<UnityEngine.AI.NavMeshAgent>();
                    meshAgent.enabled = false;

                    player.transform.position = teleportPoint.position;
                    player.transform.localRotation = teleportPoint.localRotation;

                    meshAgent.enabled = true;
                }
            }
            if (portalStones2.localPosition.y < 1.4)
            {
                portalStones2.Translate(new Vector3(0, 1.4f, 0) * Time.fixedDeltaTime, Space.World);
            }
        }
        else if (!animated)
        {
            if (portalStones.localPosition.y > 0)
            {
                portalStones.Translate(new Vector3(0, -1, 0) * Time.fixedDeltaTime, Space.World);
                _particleSystem.Stop();
            }
            else if (portalStones.localPosition.y < 0)
            {
                portalStones.localPosition = new Vector3(0, 0, 0);
            }
            if (portalStones2.localPosition.y > 0)
            {
                portalStones2.Translate(new Vector3(0, -1.4f, 0) * Time.fixedDeltaTime, Space.World);
            }
            else if (portalStones2.localPosition.y < 0)
            {
                portalStones2.localPosition = new Vector3(0, 0, 0);
            }
        }

        rotationSpeed = RotationSpeed * portalStones.localPosition.y;
        if (animated)
        {
            portalStones.Rotate(0f, rotationSpeed, 0f, Space.World);
            portalStones2.Rotate(0f, -rotationSpeed, 0f, Space.World);
        }
        else if (!animated && portalStones.localPosition.y > 0.00000001f)
        {
            portalStones.localEulerAngles = portalStones2.localEulerAngles - Quaternion.Euler(0, 0, 45 * (1f - portalStones.localPosition.y)).eulerAngles * 0.4f;
            portalStones2.localEulerAngles = portalStones.localEulerAngles - Quaternion.Euler(0, 0, 45 * (1f - portalStones.localPosition.y)).eulerAngles * 0.4f;
        }
        else
        {
            portalStones.localEulerAngles = portalStones2.localEulerAngles - Quaternion.Euler(0, 0, 45).eulerAngles;
        }
    }

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            animated = true;
            if (player == null)
                player = col.gameObject;
            timeLeft = delay;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            animated = false;
        }
    }
}
