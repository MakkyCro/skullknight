using UnityEngine;

public class CameraController : MonoBehaviour {

	public Transform target;
    public Vector3 offset;

    private float currentX = 0f;
    private float currentY = 0f;
    private float distance;

    public float sensitivityX = 50f;
    public float sensitivityY = 45f;

	public float currentZoom = 1f;
	public float maxZoom = 4f;
	public float minZoom = 0.5f;
	public float zoomSensitivity = 1.5f;

    private float delay = 1f;

	float zoomSmoothV;
	float targetZoom;

    void Start()
    {
        offset = transform.localPosition;
        targetZoom = currentZoom;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
	}


    // Mouse zooming
	void Update()
	{
        if (delay < 0)
        {
            if (!InventoryUI.opened)
            {
                float scroll = Input.GetAxisRaw("Mouse ScrollWheel") * zoomSensitivity;

                if (scroll != 0f)
                {
                    targetZoom = Mathf.Clamp(targetZoom - scroll, minZoom, maxZoom);
                }
                currentZoom = Mathf.SmoothDamp(currentZoom, targetZoom, ref zoomSmoothV, .15f);

                currentX += Input.GetAxis("Mouse X") * sensitivityX * Time.deltaTime;
                currentY -= Input.GetAxis("Mouse Y") * sensitivityY * Time.deltaTime;

                currentY = Mathf.Clamp(currentY, -50f, 50f);
            }
        }
        else
            delay -= Time.deltaTime;
    }

    void LateUpdate()
    {
        // ne gledajte komentare, to je samo nesto bezveze
        ////transform.position = target.position - transform.forward * dst * currentZoom;
        ////transform.LookAt(target.position);

        //float horizontal = Input.GetAxis("Mouse X") * mouseLookSpeed;
        //float vertical = Input.GetAxis("Mouse Y") * mouseLookSpeed;
        //transform.RotateAround(target.position, new Vector3 (0, 1, 0), horizontal);
        //transform.RotateAround(target.position, new Vector3 (1, 0, 0), vertical);

        ////float desiredAngle = transform.eulerAngles.y;
        ////Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
        //transform.position = target.transform.position - (offset);

        ////transform.LookAt(target.transform);

        ////float yawInput = Input.GetAxisRaw("Horizontal");
        ////transform.RotateAround(target.position, Vector3.up, -yawInput * yawSpeed * Time.deltaTime);

        Vector3 dir = new Vector3(0, 0, offset.z) * currentZoom;
        //Vector3 dir = offset * currentZoom;
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        //Vector3 desiredPosition = target.position + rotation * dir;

        //RaycastHit hit;

        //Vector3 direction1 = (desiredPosition - target.localPosition);
        //Debug.Log(desiredPosition);
        //Debug.Log(target.position);
        //Debug.Log(direction1*20f);
        //Debug.DrawLine(target.position, transform.TransformDirection(direction1.normalized) * 3f, Color.red);
        //if (Physics.Raycast(target.position, direction1, out hit, maxZoom))
        //{
        //    distance = Mathf.Clamp(hit.distance * 0.85f, offset.z*minZoom, offset.z*maxZoom);   
        //    Debug.DrawLine(target.position, hit.point, Color.blue);
        //}
        //else
        //{
        //    distance = offset.z * currentZoom;
        //}




        transform.position = target.position + rotation * dir;
        //transform.localPosition = Vector3.Lerp(transform.localPosition, dir.normalized * distance, Time.deltaTime);
        transform.LookAt(target.transform);
        transform.Translate(offset.x * currentZoom, 0, 0, Space.Self);




    }

    //public class CollisionDetector
    //{
    //    [HideInInspector]
    //    public bool colliding = false;
    //    [HideInInspector]
    //    public Vector3[] adjustedCameraClipPoints;
    //    [HideInInspector]
    //    public Vector3[] desiredCameraClipPoints;

    //    Camera camera;
    //    public void Initialize(Camera cam)
    //    {
    //        camera = cam;
    //        adjustedCameraClipPoints = new Vector3[5];
    //        desiredCameraClipPoints = new Vector3[5];
    //    }

    //    public void UpdateCameraClipPoints(Vector3 camPos, Quaternion atRotation, ref Vector3[] intoArray)
    //    {
    //        if (!camera)
    //            return;

    //        intoArray = new Vector3[5];

    //        float z = camera.nearClipPlane;
    //        float x = Mathf.Tan(camera.fieldOfView / 3.41f) * z;
    //        float y = x / camera.aspect;

    //        intoArray[0] = (atRotation * new Vector3(-x, y, z)) + camPos;
    //        intoArray[1] = (atRotation * new Vector3(x, y, z)) + camPos;
    //        intoArray[2] = (atRotation * new Vector3(-x, -y, z)) + camPos;
    //        intoArray[3] = (atRotation * new Vector3(x, -y, z)) + camPos;
    //        intoArray[4] = camPos - camera.transform.forward;
    //    }

    //    bool CollisionDetectedAtClipPoints(Vector3[] clipPoints, Vector3 fromPosition)
    //    {
    //        for(int i = 0; i < clipPoints.Length; i++)
    //        {
    //            Ray ray = new Ray(fromPosition, clipPoints[i] - fromPosition);
    //            float distance = Vector3.Distance(clipPoints[i], fromPosition);
    //            if(Physics.Raycast(ray, distance))
    //            {
    //                return true;
    //            }
    //        }
    //        return false;
    //    }


    //    public float GetAdjustedDistanceWithRayFrom(Vector3 from)
    //    {
    //        float distance = -1f;

    //        for (int i = 0; i < desiredCameraClipPoints.Length; i++)
    //        {
    //            Ray ray = new Ray(from, desiredCameraClipPoints[i] - from);
    //            RaycastHit hit;
    //            if(Physics.Raycast(ray, out hit))
    //            {
    //                if (distance == -1f)
    //                    distance = hit.distance;
    //                else
    //                {
    //                    if(hit.distance < distance)
    //                        distance = hit.distance;
    //                }
    //            }
    //        }

    //        if (distance == -1f)
    //            return 0;
    //        else
    //            return distance;
    //    }

    //    public void CheckColliding(Vector3 targetPosition)
    //    {
    //        if(CollisionDetectedAtClipPoints(desiredCameraClipPoints, targetPosition))
    //        {
    //            colliding = true;
    //        }
    //        else
    //        {
    //            colliding = false;
    //        }
    //    }
    //}


}
