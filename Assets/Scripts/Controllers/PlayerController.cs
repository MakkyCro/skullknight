using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	public delegate void OnFocusChanged(Interactable newFocus);
	public OnFocusChanged onFocusChangedCallback;

	public Interactable focus;	// Our current focus: Item, Enemy etc.

	public LayerMask movementMask;		// The ground
	public LayerMask interactionMask;   // Everything we can interact with

    private ColorOnHover focusedObject = null;
    private PlayerAnimator playerAnimator = null;
    private CameraController camC;
    private Transform camLookTarget;

    public static bool Attacked;

    public float moveSpeed = 3f; 

    Animator animator;
	Camera cam;				// Reference to our camera


    private float coolDown = 0f;
    private float attackCooldown = 0f;
    [SerializeField]
    public GameObject tutorialObject;

    // Get references
    void Start()
	{
        //Time.timeScale = 0;
        animator = GetComponentInChildren<Animator>();
        playerAnimator = GetComponentInChildren<PlayerAnimator>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
        camC = cam.GetComponent<CameraController>();
	}

	
	void Update() {

		if (EventSystem.current.IsPointerOverGameObject())
			return;

        if (coolDown != 0f)
        {
            coolDown -= Time.deltaTime;
            if (coolDown <= 0f)
            {
                coolDown = 0f;
                focus.OnDefocused();
                SetFocus(null);
            }
        }

        if(attackCooldown != 0f)
        {
            attackCooldown -= Time.deltaTime;
            if (attackCooldown <= 0f)
            {
                Attacked = false;
                attackCooldown = 0f;
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            if(attackCooldown == 0f)
            {
                Attacked = true;
                playerAnimator.OnAttack();
                attackCooldown = 0.8f;
            }
        }

        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.5f));
        RaycastHit hit;
        
        float currentZoom = camC.currentZoom;

        bool target = Physics.Raycast(ray, out hit, 3f * Mathf.Clamp(camC.currentZoom, 1f, 4f), interactionMask);

        // If we hit
        if (target)
        {
            focusedObject = hit.collider.GetComponent<ColorOnHover>();
            focusedObject.Highlight();
        }
        else if(focusedObject != null)
        {
            focusedObject.DeHighlight();
            focusedObject = null;
        }

		// If we press e to interact and we have a target
		if (Input.GetKey(KeyCode.E) && target)
		{
            SetFocus(hit.collider.GetComponent<Interactable>());
            coolDown = 3f;
        }

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        if(h != 0 || v != 0)
        {
            Vector3 angles = cam.transform.rotation.eulerAngles;
            float inputMax = Mathf.Max(Mathf.Abs(h), Mathf.Abs(v));

            if (h < 0f && v == 0f)
                angles.y -= 90f;
            else if (h > 0f && v == 0f)
                angles.y += 90f;
            else if (h < 0f && v < 0f)
                angles.y -= 135f;
            else if (h < 0f && v > 0f)
                angles.y -= 45f;
            else if (h > 0f && v < 0f)
                angles.y += 135f;
            else if (h > 0f && v > 0f)
                angles.y += 45f;
            else if (v < 0f && h == 0f)
                angles.y -= 180f;



            angles.x = 0f;
            angles.z = 0f;
            Quaternion newRotation = Quaternion.Euler(angles);
            gameObject.transform.rotation = newRotation;
            
            gameObject.transform.Translate(new Vector3(0, 0, inputMax) * moveSpeed * Time.deltaTime);
            animator.SetFloat("Speed Percent", inputMax, .1f, Time.deltaTime);
        }
        else
            animator.SetFloat("Speed Percent", 0, .1f, Time.deltaTime);

        if (Input.GetKeyDown("space") && tutorialObject.activeSelf)
        {
            tutorialObject.SetActive(false);
            //Time.timeScale = 1;
        }

    }

    // Setting our focus to a new focus
    void SetFocus(Interactable newFocus)
    {
        if (onFocusChangedCallback != null)
            onFocusChangedCallback.Invoke(newFocus);

        // Focus has been changed
        if (focus != newFocus && focus != null)
        {
            // Defocusing our previous focus
            focus.OnDefocused();
        }

        // Set our focus to what we hit
        // Not interactable set to null
        focus = newFocus;

        if (focus != null)
        {
            // Let our focus know that it's being focused
            focus.OnFocused(transform);
        }

    }

}
